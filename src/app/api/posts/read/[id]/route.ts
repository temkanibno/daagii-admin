import { getPost, increaseReadCount } from "@admin/services/post.service";
import { getPathValue } from "@admin/utils/get-path-value";
import { NextRequest, NextResponse } from "next/server";

export async function GET(request: NextRequest) {
  try {
    const id = getPathValue(request, "posts/read");
    const { result, error } = await getPost(id);
    await increaseReadCount(id);
    console.error("error:", error);
    if (error) throw error;
    return NextResponse.json({ result }, { status: 200 });
  } catch (error: any) {
    return NextResponse.json({ error: error.message }, { status: 500 });
  }
}

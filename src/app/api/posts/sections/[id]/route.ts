import { deletePostSection, getPostSection, updatePostSection } from "@admin/services/post-section.service";
import { getPathValue } from "@admin/utils/get-path-value";
import { NextRequest, NextResponse } from "next/server";
import slugify from "slugify";

export async function GET(request: NextRequest) {
  try {
    const id = getPathValue(request, "posts/sections");
    const { result, error } = await getPostSection(id);
    if (error) throw error;
    return NextResponse.json({ result }, { status: 200 });
  } catch (error: any) {
    return NextResponse.json({ error: error.message }, { status: 500 });
  }
}

export async function PATCH(request: NextRequest) {
  try {
    const id = getPathValue(request, "posts/sections");
    const body = await request.json();
    body.slug = slugify(body.title).toLowerCase();
    delete body._id;
    const { result, error } = await updatePostSection(id, body);
    if (error) throw error;
    return NextResponse.json({ result }, { status: 200 });
  } catch (error: any) {
    return NextResponse.json({ error: error.message }, { status: 500 });
  }
}

export async function DELETE(request: NextRequest) {
  try {
    const id = getPathValue(request, "posts/sections");
    const { result, error } = await deletePostSection(id);
    if (error) throw error;
    return NextResponse.json({ result }, { status: 200 });
  } catch (error: any) {
    return NextResponse.json({ error: error.message }, { status: 500 });
  }
}

import { createPostSection, getAllPostSections } from "@admin/services/post-section.service";
import { NextRequest, NextResponse } from "next/server";
import slugify from "slugify";

export async function POST(request: NextRequest) {
  try {
    const body = await request.json();
    body.slug = slugify(body.title).toLowerCase();
    const { result, error } = await createPostSection(body);
    if (error) throw error;
    return NextResponse.json({ result }, { status: 200 });
  } catch (error: any) {
    return NextResponse.json({ error: error.message }, { status: 500 });
  }
}

export async function GET() {
  try {
    const { result, error } = await getAllPostSections();
    if (error) throw error;

    return NextResponse.json({ result }, { status: 200 });
  } catch (error: any) {
    return NextResponse.json({ error: error.message }, { status: 500 });
  }
}

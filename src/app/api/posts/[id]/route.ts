import { deletePost, getPost, updatePost } from "@admin/services/post.service";
import { getPathValue } from "@admin/utils/get-path-value";
import { NextRequest, NextResponse } from "next/server";
import slugify from "slugify";

export async function GET(request: NextRequest) {
  try {
    const id = getPathValue(request, "posts");
    const { result, error } = await getPost(id);
    if (error) throw error;
    return NextResponse.json({ result }, { status: 200 });
  } catch (error: any) {
    return NextResponse.json({ error: error.message }, { status: 500 });
  }
}

export async function PATCH(request: NextRequest) {
  try {
    const id = getPathValue(request, "posts");
    const body = await request.json();
    body.slug = slugify(body.title);
    const { result, error } = await updatePost(id, body);
    if (error) throw error;
    return NextResponse.json({ result }, { status: 200 });
  } catch (error: any) {
    console.error(error.message);
    return NextResponse.json({ error: error.message }, { status: 500 });
  }
}

export async function DELETE(request: NextRequest) {
  try {
    const id = getPathValue(request, "posts");
    const { result, error } = await deletePost(id);
    if (error) throw error;
    return NextResponse.json({ result }, { status: 200 });
  } catch (error: any) {
    return NextResponse.json({ error: error.message }, { status: 500 });
  }
}

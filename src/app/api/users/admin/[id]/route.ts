import { toggleUserAdmin } from "@admin/services/user.service";
import { getPathValue } from "@admin/utils/get-path-value";
import { NextRequest, NextResponse } from "next/server";

export async function PATCH(request: NextRequest) {
  try {
    const id = getPathValue(request, "users/admin");
    const { result } = await toggleUserAdmin(id);
    return NextResponse.json({ result }, { status: 200 });
  } catch (error: any) {
    return NextResponse.json({ error: error.message }, { status: 500 });
  }
}

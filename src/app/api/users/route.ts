import { getAllUsers } from "@admin/services/user.service";
import { NextResponse } from "next/server";

export async function GET() {
  try {
    const { result, error } = await getAllUsers();
    if (error) throw error;

    return NextResponse.json({ result }, { status: 200 });
  } catch (error: any) {
    return NextResponse.json({ error: error.message }, { status: 500 });
  }
}

"use client";
import { Box, Button, Stack, Typography } from "@mui/material";

const Page = () => {
  return (
    <>
      <Box
        sx={{
          backgroundColor: "background.paper",
          flex: "1 1 auto",
          alignItems: "center",
          display: "flex",
          justifyContent: "center",
        }}
      >
        <Box
          sx={{
            maxWidth: 550,
            px: 3,
            py: "100px",
            width: "100%",
          }}
        >
          <Box>
            <Stack spacing={1} sx={{ mb: 3 }}>
              <Typography variant="h4" textAlign={"center"}>
                Нэвтрэх
              </Typography>
            </Stack>

            <Button fullWidth size="large" sx={{ mt: 3 }} type="submit" variant="contained" href="/api/auth/login">
              Үргэлжлүүлэх
            </Button>
          </Box>
        </Box>
      </Box>
    </>
  );
};

export default Page;

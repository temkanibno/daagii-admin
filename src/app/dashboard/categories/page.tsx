"use client";
import { CategoriesCreate } from "@admin/components/categories/categories-create";
import { CategoriesEdit } from "@admin/components/categories/categories-edit";
import { CategoriesTable } from "@admin/components/categories/categories-table";
import { usePage } from "@admin/hooks/use-page";
import { PlusIcon } from "@heroicons/react/24/solid";
import { Box, Button, Container, Stack, SvgIcon, Typography } from "@mui/material";

export default function Home() {
  const { handleCreate, isCreating, editingItem, handleSubmit, handleClose, ...tableProps } = usePage("/api/posts/sections");
  return (
    <Box
      component="main"
      sx={{
        flexGrow: 1,
        py: 8,
      }}
    >
      <Container maxWidth="xl">
        <Stack spacing={3}>
          <Stack direction="row" justifyContent="space-between" spacing={4}>
            <Stack spacing={1}>
              <Typography variant="h4">Мэдээний ангилал</Typography>
            </Stack>
            <Box>
              <Button
                startIcon={
                  <SvgIcon fontSize="small">
                    <PlusIcon />
                  </SvgIcon>
                }
                variant="contained"
                onClick={handleCreate}
              >
                Бүртгэх
              </Button>
            </Box>
          </Stack>
          <CategoriesTable {...tableProps} />
          {isCreating && <CategoriesCreate onSubmit={handleSubmit} onClose={handleClose} />}
          {editingItem && <CategoriesEdit values={editingItem} onSubmit={handleSubmit} onClose={handleClose} />}
        </Stack>
      </Container>
    </Box>
  );
}

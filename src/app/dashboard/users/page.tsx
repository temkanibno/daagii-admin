"use client";
import { UsersTable } from "@admin/components/users/users-table";
import { usePage } from "@admin/hooks/use-page";
import { Box, Container, Stack, Typography } from "@mui/material";

export default function Home() {
  const { handleCreate, handleSubmit, handleClose, ...tableProps } = usePage("/api/users");
  return (
    <Box
      component="main"
      sx={{
        flexGrow: 1,
        py: 8,
      }}
    >
      <Container maxWidth="xl">
        <Stack spacing={3}>
          <Stack direction="row" justifyContent="space-between" spacing={4}>
            <Stack spacing={1}>
              <Typography variant="h4">Хэрэглэгчид</Typography>
            </Stack>
          </Stack>
          <UsersTable {...tableProps} />
        </Stack>
      </Container>
    </Box>
  );
}

import { AdminLayout } from "@admin/layouts/dashboard/layout";

export default function Layout(props: any) {
  const { children } = props;
  return (
    <html lang="en">
      <body>
        <AdminLayout>{children}</AdminLayout>
      </body>
    </html>
  );
}

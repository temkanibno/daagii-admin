"use client";
import { ArticlesCreate } from "@admin/components/articles/articles-create";
import { ArticlesEdit } from "@admin/components/articles/articles-edit";
import { ArticlesTable } from "@admin/components/articles/articles-table";
import { usePage } from "@admin/hooks/use-page";
import { fetcher } from "@admin/utils/fetcher";
import { PlusIcon } from "@heroicons/react/24/solid";
import { Box, Button, Container, Stack, SvgIcon, Typography } from "@mui/material";
import useSWR from "swr";

export default function Home() {
  const { data: sections } = useSWR("/api/posts/sections", fetcher);
  const { handleCreate, isCreating, editingItem, isLoading, handleSubmit, handleClose, ...tableProps } = usePage("/api/posts");
  const listShown = !isCreating && !editingItem;
  if (isLoading) {
    return <>Loading...</>;
  }
  return (
    <Box
      component="main"
      sx={{
        flexGrow: 1,
        py: 8,
      }}
    >
      <Container maxWidth="xl">
        {listShown && <List tableProps={tableProps} handleCreate={handleCreate} />}
        {isCreating && <ArticlesCreate sections={sections} onSubmit={handleSubmit} onClose={handleClose} />}
        {editingItem && <ArticlesEdit values={editingItem} sections={sections} onSubmit={handleSubmit} onClose={handleClose} />}
      </Container>
    </Box>
  );
}
const List = ({ handleCreate, tableProps }: any) => {
  return (
    <Stack spacing={3}>
      <Stack direction="row" justifyContent="space-between" spacing={4}>
        <Stack spacing={1}>
          <Typography variant="h4">Мэдээ мэдээлэл</Typography>
        </Stack>
        <Box>
          <Button
            startIcon={
              <SvgIcon fontSize="small">
                <PlusIcon />
              </SvgIcon>
            }
            variant="contained"
            onClick={handleCreate}
          >
            Бүртгэх
          </Button>
        </Box>
      </Stack>
      <ArticlesTable {...tableProps} />
    </Stack>
  );
};

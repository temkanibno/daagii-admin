"use client";
import { ImagesCreate } from "@admin/components/images/images-create";
import { ImagesList } from "@admin/components/images/images-list";
import { usePage } from "@admin/hooks/use-page";
import { PlusIcon } from "@heroicons/react/24/solid";
import { Box, Button, Container, Stack, SvgIcon, Typography } from "@mui/material";

export default function Home() {
  const { handleCreate, isCreating, handleSubmit, handleClose, ...tableProps } = usePage("/api/files", 10);
  return (
    <Box
      component="main"
      sx={{
        flexGrow: 1,
        py: 8,
      }}
    >
      <Container maxWidth="xl">
        <Stack spacing={3}>
          <Stack direction="row" justifyContent="space-between" spacing={4}>
            <Stack spacing={1}>
              <Typography variant="h4">Зурагын сан</Typography>
            </Stack>
            <Box>
              <Button
                startIcon={
                  <SvgIcon fontSize="small">
                    <PlusIcon />
                  </SvgIcon>
                }
                variant="contained"
                onClick={handleCreate}
              >
                Бүртгэх
              </Button>
            </Box>
          </Stack>
          <ImagesList {...tableProps} />
          {isCreating && <ImagesCreate onSubmit={handleSubmit} onClose={handleClose} />}
        </Stack>
      </Container>
    </Box>
  );
}

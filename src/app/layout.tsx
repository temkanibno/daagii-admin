import ThemeRegistry from "./PageRegistry";
import "simplebar-react/dist/simplebar.min.css";
import { ToastProvider } from "@admin/contexts/toast-context";
import { UserProvider } from "@admin/contexts/user-context";

export default function RootLayout(props: any) {
  const { children } = props;
  return (
    <html lang="en">
      <body>
        <ThemeRegistry options={{ key: "mui" }}>
          <ToastProvider>
            <UserProvider>{children}</UserProvider>
          </ToastProvider>
        </ThemeRegistry>
      </body>
    </html>
  );
}

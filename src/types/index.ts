export interface BaseTableProps {
  totalItems?: number;
  pageItems?: any[]; // You can replace 'any' with a specific type if you have one
  handlePageChange?: (page: number) => void;
  handleRowsPerPageChange?: (rowsPerPage: number) => void;
  page?: number;
  rowsPerPage?: number;
}

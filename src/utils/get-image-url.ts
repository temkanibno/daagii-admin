import { Image } from "@prisma/client";

type ImageSize = "extra_small" | "small" | "medium" | "big" | "large" | "original";

export const getImageUrl = (image: Image, size: ImageSize = "medium"): string => {
  if (!image) return "";
  if (size === "original") return `https://${image.bucket}.s3.${image.region}.amazonaws.com/${image.key}${image.id}_source.${image.originalExt}`;
  return `https://${image.bucket}.s3.${image.region}.amazonaws.com/${image.key}${image.id}_extra_small.webp`;
};

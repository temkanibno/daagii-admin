import { useRouter } from "next/navigation";
import { Box, Divider, MenuItem, MenuList, Popover, Typography } from "@mui/material";
import { useUser } from "@admin/hooks/use-user";

export const AccountPopover = (props: any) => {
  const { anchorEl, onClose, open } = props;
  const router = useRouter();
  const { user } = useUser();

  return (
    <Popover
      anchorEl={anchorEl}
      anchorOrigin={{
        horizontal: "left",
        vertical: "bottom",
      }}
      onClose={onClose}
      open={open}
    >
      <Box
        sx={{
          py: 1.5,
          px: 2,
        }}
      >
        <Typography variant="overline">Хэрэглэгч</Typography>
        <Typography color="text.secondary" variant="body2">
          {user?.name}
        </Typography>
      </Box>
      <Divider />
      <MenuList
        disablePadding
        dense
        sx={{
          p: "8px",
          "& > *": {
            borderRadius: 1,
          },
        }}
      >
        <MenuItem onClick={() => router.push("/api/auth/logout")}>Гарах</MenuItem>
      </MenuList>
    </Popover>
  );
};

import { useContext } from "react";
import { AlertColor } from "@mui/material";
import { ToastContext } from "@admin/contexts/toast-context";

const DEFAULT_AUTO_HIDE_DURATION = 3000;

export const useToast = () => {
  const { setSnack } = useContext(ToastContext);

  const showToast =
    (color: AlertColor) =>
    (message: string, autoHideDuration = DEFAULT_AUTO_HIDE_DURATION) => {
      setSnack({ message, autoHideDuration, open: true, color });
    };

  return {
    success: showToast("success"),
    error: showToast("error"),
    info: showToast("info"),
    warning: showToast("warning"),
  };
};

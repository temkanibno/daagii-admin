"use client";

import { User } from "@prisma/client";
import { createContext, useState } from "react";

type SnackDefaultValue = {
  user: User | undefined;
  setUser: React.Dispatch<React.SetStateAction<User | undefined>>;
};

export const UserContext = createContext<SnackDefaultValue>({} as SnackDefaultValue);

interface UserProviderProps {
  children: React.ReactNode;
}

export const UserProvider: React.FC<UserProviderProps> = ({ children }) => {
  const [user, setUser] = useState<User | undefined>(undefined);

  return <UserContext.Provider value={{ user, setUser }}>{children}</UserContext.Provider>;
};

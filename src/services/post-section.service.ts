import { Prisma } from "@prisma/client";
import prisma from "../utils/db";

export const getPostSection = async (id: string) => {
  try {
    const result = await prisma.postSection.findUnique({ where: { id } });
    return { result };
  } catch (error) {
    return { error };
  }
};

export const getAllPostSections = async () => {
  try {
    const result = await prisma.postSection.findMany();
    return { result };
  } catch (error) {
    return { error };
  }
};

export const createPostSection = async (data: Prisma.PostSectionCreateInput) => {
  try {
    const result = await prisma.postSection.create({ data });
    return { result };
  } catch (error) {
    return { error };
  }
};

export const updatePostSection = async (id: string, data: Prisma.PostSectionUpdateInput) => {
  try {
    const result = await prisma.postSection.update({ where: { id }, data });
    return { result };
  } catch (error) {
    return { error };
  }
};

export const deletePostSection = async (id: string) => {
  try {
    const result = await prisma.postSection.delete({ where: { id } });
    return { result: result.id };
  } catch (error) {
    return { error };
  }
};

export const updatePostCount = async (id: string) => {
  try {
    const postCount = await prisma.post.count({ where: { sectionId: id } });
    const result = await prisma.postSection.update({ where: { id }, data: { postCount } });
    return { result };
  } catch (error) {
    return { error };
  }
};

import { CircularProgress, Stack } from "@mui/material";

export const Loader = () => {
  return (
    <Stack sx={{ position: "fixed", inset: 0 }} justifyContent={"center"} alignItems={"center"}>
      <CircularProgress />
    </Stack>
  );
};

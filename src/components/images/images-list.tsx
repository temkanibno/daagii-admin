import { Button, Card, CardActions, CardMedia, IconButton, Pagination, Stack, SvgIcon, Tooltip } from "@mui/material";
import { BaseTableProps } from "@admin/types";
import Grid from "@mui/material/Unstable_Grid2"; // Grid version 2
import { nanoid } from "nanoid";
import { LinkIcon } from "@heroicons/react/24/solid";
import { useToast } from "@admin/hooks/use-toast";
import { getImageUrl } from "@admin/utils/get-image-url";
import { Image } from "@prisma/client";

export const ImagesList: React.FC<BaseTableProps> = (props) => {
  const { totalItems = 0, pageItems = [], handlePageChange = () => {}, page = 0, rowsPerPage = 10 } = props;
  console.log("totalItems:", totalItems);
  console.log("rowsPerPage:", rowsPerPage);
  const toast = useToast();

  return (
    <Card sx={{ padding: 4, boxShadow: "0 0 10px rgba(0,0,0,.5)" }}>
      <Stack rowGap={4}>
        <Grid container rowSpacing={1} columnSpacing={{ xs: 1, sm: 2, md: 3 }} columns={12}>
          {pageItems.map((row: Image) => (
            <Grid md={2} sm={4} xs={6} key={nanoid()}>
              <Card sx={{ border: 0, borderRadius: 0 }}>
                <CardMedia component="img" height={200} image={getImageUrl(row, "extra_small")} alt="Paella dish" />
                <CardActions sx={{ display: "flex", justifyContent: "flex-end" }}>
                  <Button fullWidth onClick={() => navigator.clipboard.writeText(getImageUrl(row, "original"))}>
                    <SvgIcon fontSize="small">
                      <LinkIcon />
                    </SvgIcon>
                    &nbsp; Холбоос хуулах
                  </Button>
                </CardActions>
              </Card>
            </Grid>
          ))}
        </Grid>
        <Pagination
          sx={{ display: "flex", justifyContent: "flex-end" }}
          onChange={(_, page) => {
            handlePageChange(page - 1);
          }}
          page={page}
          count={Math.ceil(totalItems / rowsPerPage)}
          shape="rounded"
        />
      </Stack>
    </Card>
  );
};

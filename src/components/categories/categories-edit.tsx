import { Button, Dialog, DialogActions, DialogContent, DialogTitle, Stack, TextField } from "@mui/material";
import { PostSection } from "@prisma/client";
import axios from "axios";
import { useFormik } from "formik";
import * as Yup from "yup";

interface CategoriesEditProps {
  values: PostSection;
  onSubmit: () => void;
  onClose: (data: any) => void;
}

export const CategoriesEdit: React.FC<CategoriesEditProps> = (props) => {
  const { onSubmit, onClose, values: rawValues } = props;
  const formik = useFormik({
    initialValues: {
      title: rawValues?.title + "" || "",
    },
    validationSchema: Yup.object({
      title: Yup.string().max(255).required("Нэр хоосон байсан болохгүй"),
    }),
    onSubmit: async (values, helpers) => {
      try {
        const { data } = await axios.patch(`/api/posts/sections/${rawValues.id}`, values);
        onSubmit();
      } catch (err: any) {}
    },
  });
  return (
    <Dialog open={true} onClose={onClose} maxWidth={"sm"} fullWidth>
      <form noValidate onSubmit={formik.handleSubmit}>
        <DialogTitle id="alert-dialog-title">Мэдээний ангилал засах</DialogTitle>
        <DialogContent>
          <Stack spacing={3}>
            <TextField name="title" error={!!(formik.touched.title && formik.errors.title)} fullWidth helperText={formik.touched.title && formik.errors.title} label="Нэр" onBlur={formik.handleBlur} onChange={formik.handleChange} type="text" value={formik.values.title} />
          </Stack>
        </DialogContent>
        <DialogActions>
          <Button onClick={onClose} type="button">
            Болих
          </Button>
          <Button type="submit" variant="contained" autoFocus>
            Засах
          </Button>
        </DialogActions>
      </form>
    </Dialog>
  );
};

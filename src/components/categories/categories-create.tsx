import { Button, Dialog, DialogActions, DialogContent, DialogTitle, Stack, TextField } from "@mui/material";
import { useFormik } from "formik";
import { useState } from "react";
import * as Yup from "yup";
import { Loader } from "../loader";
import axios from "axios";

interface CategoriesCreateProps {
  onSubmit: () => void;
  onClose: (data: any) => void;
}

export const CategoriesCreate: React.FC<CategoriesCreateProps> = (props) => {
  const { onSubmit, onClose } = props;
  const [loading, setLoading] = useState(false);
  const formik = useFormik({
    initialValues: {
      title: "",
    },
    validationSchema: Yup.object({
      title: Yup.string().max(255).required("Нэр хоосон байсан болохгүй"),
    }),
    onSubmit: async (values) => {
      try {
        setLoading(true);
        const { data } = await axios.post("/api/posts/sections", { ...values });
        onSubmit();
        setLoading(false);
      } catch (err: any) {}
    },
  });
  return (
    <Dialog open={true} onClose={onClose} maxWidth={"sm"} fullWidth>
      {loading && <Loader />}
      <form noValidate onSubmit={formik.handleSubmit}>
        <DialogTitle id="alert-dialog-title">Мэдээний ангилал үүсгэх</DialogTitle>
        <DialogContent>
          <Stack spacing={3}>
            <TextField name="title" error={!!(formik.touched.title && formik.errors.title)} fullWidth helperText={formik.touched.title && formik.errors.title} label="Нэр" onBlur={formik.handleBlur} onChange={formik.handleChange} type="text" value={formik.values.title} />
          </Stack>
        </DialogContent>
        <DialogActions>
          <Button onClick={onClose} type="button">
            Болих
          </Button>
          <Button type="submit" variant="contained" autoFocus>
            Үүсгэх
          </Button>
        </DialogActions>
      </form>
    </Dialog>
  );
};
